package com.example.jonathan.github.presenter;

import android.util.Log;

import com.example.jonathan.github.data.model.GithubEntity;
import com.example.jonathan.github.data.model.Item;
import com.example.jonathan.github.data.network.api.GithubAPI;
import com.example.jonathan.github.view.BaseView;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jonathan on 21/11/16.
 */

public class GithubMainPresenter {

    private final String TAG = "GitMainPresenter";

    private BaseView<Item> mGithubMainView;

    // Controla o número de páginas que já foram carregadas
    private int mPageCount = 1;

    private ArrayList<Item> mList;

    public GithubMainPresenter(BaseView mainView) {
        mGithubMainView = mainView;
        mList = new ArrayList();
    }

    public void load() {
        final boolean firstLoad = mList.size() <=0;
        final int currentPage = firstLoad ? 1 : mPageCount;

        load(currentPage);
    }

    public void load(final int page) {
        mPageCount = page;

        mGithubMainView.onHideRetry();

        GithubAPI api = GithubAPI.getInstance();

        api.getRepositories(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GithubEntity>() {
                    @Override
                    public void onCompleted() {
                        //
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGithubMainView.onShowRetry();
                        Log.d(TAG, "Erro no load(page). " + e.getMessage());
                    }

                    @Override
                    public void onNext(GithubEntity githubEntity) {
                        if(githubEntity != null)
                            setList(githubEntity.getItems());
                    }
                });
    }

    public ArrayList<Item> getList() {
        return mList;
    }

    public void setList(List<Item> itemList) {
        mList.addAll(itemList);

        mGithubMainView.onRenderData(mList);
    }

    public void onItemClick(final int position) {
        mGithubMainView.onItemSelected(mList.get(position));
    }
}
