package com.example.jonathan.github.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.jonathan.github.R;
import com.example.jonathan.github.adapter.GithubMainAdapter;
import com.example.jonathan.github.data.model.Item;
import com.example.jonathan.github.presenter.GithubMainPresenter;
import com.example.jonathan.github.util.OnClickGeneric;
import com.example.jonathan.github.util.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragmento principal responsável por exibir os repositórios
 * mais populares de Java
 *
 * Created by jonathan on 22/11/16.
 */

public class GithubFragment extends Fragment implements BaseView<Item> {
    private final String TAG = "GithubFragment";

    @BindView(R.id.rvGithubList) RecyclerView rvGithubList;
    @BindView(R.id.rl_progress) RelativeLayout rlProgress;
    @BindView(R.id.rl_retry) RelativeLayout rlRetry;

    private GithubMainAdapter mAdapter;

    private GithubMainPresenter mPresenter;

    private final String KEY_RECYCLER_STATE = "recycler_state";
    private final String KEY_LIST_STATE = "list_state";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new GithubMainPresenter(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_LIST_STATE, mPresenter.getList());
        outState.putParcelable(KEY_RECYCLER_STATE, rvGithubList.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new GithubMainAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvGithubList.setLayoutManager(layoutManager);

        if(savedInstanceState != null) {
            rvGithubList.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(KEY_RECYCLER_STATE));

            ArrayList<Item> list = savedInstanceState.getParcelableArrayList(KEY_LIST_STATE);

            mPresenter.setList(list);
        }

        setupRecyclerView(layoutManager);

        rvGithubList.setAdapter(mAdapter);
    }

    private void setupRecyclerView(LinearLayoutManager layoutManager) {
        EndlessRecyclerViewScrollListener mScrollListener =
                new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mPresenter.load(page);
            }
        };

        rvGithubList.addOnScrollListener(mScrollListener);
        rvGithubList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mPresenter.onItemClick(position);
            }
        }));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_github, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onRenderData(List<Item> githubEntity) {
        mAdapter.setGithubEntity(githubEntity);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onShowRetry() {
        rlRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideRetry() {
        rlRetry.setVisibility(View.GONE);
    }

    @Override
    public void onShowErrorMessage(String message) {
        Toast.makeText(getContext(), getString(R.string.request_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(Item item) {
        Activity activity = getActivity();

        if(activity instanceof OnClickGeneric) {
            OnClickGeneric<Item> listener = (OnClickGeneric<Item>)activity;
            listener.onItemClick(item);
        }

        Log.d(TAG, item.getDescription());
    }

    @OnClick(R.id.rl_retry)
    public void retry(View view) {
        mPresenter.load();
    }
}
