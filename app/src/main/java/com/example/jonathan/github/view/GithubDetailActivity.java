package com.example.jonathan.github.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.jonathan.github.R;
import com.example.jonathan.github.data.model.Item;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan on 24/11/16.
 */

public class GithubDetailActivity extends AppCompatActivity {

    public static final String EXTRA_DETAIL = "DETAIL_PR";

    private final String REPOSITORY_NAME_STATE = "REP_NAME_STATE";

    private String mRepositoryName;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            Item item = getIntent().getParcelableExtra(EXTRA_DETAIL);

            if(item != null)
                mRepositoryName = item.getName();

            openFragment(item);
        } else {
            mRepositoryName = savedInstanceState.getString(REPOSITORY_NAME_STATE);
        }

        setupToolbar(mRepositoryName);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(REPOSITORY_NAME_STATE, mRepositoryName);
    }

    private void openFragment(Item item) {
        PullRequestFragment fragment = PullRequestFragment.newInstance(item);

        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.pull_request, fragment);
        ft.commit();
    }

    private void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    private void setupToolbar(String repositoryName) {
        setToolbarTitle(repositoryName);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }
}
