package com.example.jonathan.github.util;

/**
 * Created by jonathan on 23/11/16.
 */

public interface OnClickGeneric<T> {

    void onItemClick(T obj);
}
