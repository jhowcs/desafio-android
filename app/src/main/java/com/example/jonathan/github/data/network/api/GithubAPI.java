package com.example.jonathan.github.data.network.api;

import com.example.jonathan.github.data.model.GithubEntity;
import com.example.jonathan.github.data.model.PullRequest;
import com.example.jonathan.github.data.network.services.GithubService;

import java.util.List;

import rx.Observable;

public class GithubAPI {

    private GithubService api;

    private static GithubAPI INSTANCE;

    public static GithubAPI getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new GithubAPI();
        }

        return INSTANCE;
    }

    public Observable<GithubEntity> getRepositories(int page) {
        return api.getRepositories(page);
    }

    public Observable<List<PullRequest>> getPullRequestList(final String creator, final String repository) {
        return api.getPullRequests(creator, repository);
    }

    private GithubAPI() {
        api = BaseAPI.getRetrofit().create(GithubService.class);
    }
}
