package com.example.jonathan.github.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PullRequest implements Parcelable {

    @SerializedName("html_url")
    private String htmlUrl;

    private String title;

    private String body;

    @SerializedName("created_at")
    private String createdAt;

    private Owner user;

    protected PullRequest(Parcel in) {
        htmlUrl = in.readString();
        title = in.readString();
        body = in.readString();
        createdAt = in.readString();
        user = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(htmlUrl);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(createdAt);
        parcel.writeParcelable(user, i);
    }
}
