package com.example.jonathan.github.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.jonathan.github.R;
import com.example.jonathan.github.adapter.PullRequestAdapter;
import com.example.jonathan.github.data.model.Item;
import com.example.jonathan.github.data.model.PullRequest;
import com.example.jonathan.github.presenter.PullRequestPresenter;
import com.example.jonathan.github.util.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragmento responsável por exibir as pull requests de um repositório específico.
 *
 * Created by jonathan on 24/11/16.
 */

public class PullRequestFragment extends Fragment implements PullRequestView<PullRequest> {
    public final static String EXTRA_DETAIL = "DETAIL_PR";

    @BindView(R.id.coordinator) CoordinatorLayout coordinator;
    @BindView(R.id.rvGithubPullRequestList) RecyclerView rvPullRequest;
    @BindView(R.id.rl_progress) RelativeLayout rlProgress;
    @BindView(R.id.rl_retry) RelativeLayout rlRetry;
    @BindView(R.id.rl_empty_data) RelativeLayout rlEmptyData;

    private final String KEY_LIST_STATE = "list_state";

    private PullRequestAdapter mAdapter;

    private PullRequestPresenter mPresenter;

    public static PullRequestFragment newInstance(Item item) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_DETAIL, item);

        PullRequestFragment fragment = new PullRequestFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Item item = getArguments().getParcelable(EXTRA_DETAIL);

        mPresenter = new PullRequestPresenter(this, item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(KEY_LIST_STATE, mPresenter.getList());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pull_request, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new PullRequestAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvPullRequest.setLayoutManager(layoutManager);

        if(savedInstanceState == null) {
            mPresenter.load();
        } else {
            ArrayList<PullRequest> list = savedInstanceState.getParcelableArrayList(KEY_LIST_STATE);
            mPresenter.setList(list);
        }

        rvPullRequest.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mPresenter.onItemClick(position);
            }
        }));

        rvPullRequest.setAdapter(mAdapter);
    }

    @Override
    public void onShowProgress() {
        rlProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        rlProgress.setVisibility(View.GONE);
    }

    @Override
    public void onRenderEmptyData() {
        rlEmptyData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onShowRetry() {
        rlRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideRetry() {
        rlRetry.setVisibility(View.GONE);
    }

    @Override
    public void onRenderData(List<PullRequest> data) {
        mAdapter.setList(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onShowErrorMessage(String message) {
        Snackbar.make(coordinator, getString(R.string.request_error), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(PullRequest item) {
        openPullRequestWebPage(item.getHtmlUrl());
    }

    private void openPullRequestWebPage(String url) {
        Uri uri = Uri.parse(url);

        Intent it = new Intent(Intent.ACTION_VIEW, uri);

        startActivity(it);

    }

    @OnClick(R.id.rl_retry)
    public void retry(View view) {
        mPresenter.load();
    }
}
