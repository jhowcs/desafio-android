package com.example.jonathan.github.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jonathan on 25/11/16.
 */

public class DateFormat {

    private static final String TAG = "DateFormat";

    public static String formatDate(String dateToFormat) {
        if(dateToFormat != null) {
            String formattedDate = "";
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(dateToFormat);
                formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            } catch (ParseException e) {
                Log.e(TAG, "Erro ao formatar a data.");
            }
            return formattedDate;
        }

        return "";
    }
}
