package com.example.jonathan.github.view;

import java.util.List;

/**
 * Classe base que será implementada pelas Views para que os Presenters possam notificá-las
 *
 * Created by jonathan on 27/11/16.
 */

public interface BaseView<T>{

    void onShowRetry();

    void onHideRetry();

    void onRenderData(List<T> data);

    void onShowErrorMessage(String message);

    void onItemSelected(T item);
}
