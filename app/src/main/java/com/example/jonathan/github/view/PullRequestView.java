package com.example.jonathan.github.view;

/**
 * Estende as funcionalidades de BaseView adicionando os métodos para exibir/esconder
 * o Progress e notificar o usuário caso não exista dados para exibição
 *
 *
 * Created by jonathan on 24/11/16.
 */

public interface PullRequestView<T> extends BaseView<T> {

    void onShowProgress();

    void onHideProgress();

    void onRenderEmptyData();
}
