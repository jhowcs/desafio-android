package com.example.jonathan.github.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jonathan.github.R;
import com.example.jonathan.github.data.model.PullRequest;
import com.example.jonathan.github.util.DateFormat;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jonathan on 24/11/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestVH> {

    private List<PullRequest> mList;

    public PullRequestAdapter() {
        mList = Collections.EMPTY_LIST;
    }

    public void setList(List<PullRequest> list) {
        mList = list;
    }

    @Override
    public PullRequestVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.github_pull_request_list_row, parent, false);

        PullRequestVH vh = new PullRequestVH(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(PullRequestVH holder, int position) {
        PullRequest item = mList.get(position);

        holder.txtPullRequestName.setText(item.getTitle());
        holder.txtPullRequestBody.setText(item.getBody());
        Glide.with(holder.imgPullRequestUser.getContext())
                .load(item.getUser().getAvatarUrl())
                .into(holder.imgPullRequestUser);
        holder.txtUserName.setText(item.getUser().getLogin());
        holder.txtPullRequestDate.setText(DateFormat.formatDate(item.getCreatedAt()));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class PullRequestVH extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPullRequestName) TextView txtPullRequestName;
        @BindView(R.id.txtPullRequestBody) TextView txtPullRequestBody;
        @BindView(R.id.imgPullRequestUser) CircleImageView imgPullRequestUser;
        @BindView(R.id.txtUserName) TextView txtUserName;
        @BindView(R.id.txtPullRequestDate) TextView txtPullRequestDate;

        public PullRequestVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
