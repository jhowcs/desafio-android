package com.example.jonathan.github.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.jonathan.github.R;
import com.example.jonathan.github.data.model.Item;
import com.example.jonathan.github.util.OnClickGeneric;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan on 22/11/16.
 */

public class GithubActivity extends AppCompatActivity implements OnClickGeneric<Item> {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_github);
        ButterKnife.bind(this);

        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));
    }

    @Override
    public void onItemClick(Item obj) {
        Intent it = new Intent(this, GithubDetailActivity.class);

        it.putExtra(GithubDetailActivity.EXTRA_DETAIL, obj);

        startActivity(it);
    }
}
