package com.example.jonathan.github.data.network.services;

import com.example.jonathan.github.data.model.GithubEntity;
import com.example.jonathan.github.data.model.PullRequest;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface GithubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<GithubEntity> getRepositories(@Query("page") int page);

    @GET("repos/{creator}/{repository}/pulls")
    Observable<List<PullRequest>> getPullRequests(@Path("creator") String creator, @Path("repository") String repo);
}
