package com.example.jonathan.github.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jonathan.github.R;
import com.example.jonathan.github.data.model.Item;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan on 21/11/16.
 */

public class GithubMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<Item> mList;

    public GithubMainAdapter() {
        mList = Collections.emptyList();
    }

    public void setGithubEntity(List<Item> list) {
        mList = list;
    }

    @Override
    public int getItemViewType(int position) {
        int result = position >= (mList.size()) ? VIEW_PROG : VIEW_ITEM;
        return result;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if(viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.github_main_list_row, parent, false);
            vh = new GithubMainVH(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            vh = new ProgressViewHolder(view);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof GithubMainVH) {
            Item item = mList.get(position);

            ((GithubMainVH)holder).txtItemName.setText(item.getName());
            ((GithubMainVH)holder).txtItemDescription.setText(item.getDescription());
            ((GithubMainVH)holder).txtForkCount.setText(String.valueOf(item.getForksCount()));
            ((GithubMainVH)holder).txtStarCount.setText(String.valueOf(item.getStargazersCount()));
            ((GithubMainVH)holder).txtOwnerName.setText(item.getOwner().getLogin());
            Glide.with(((GithubMainVH)holder).imgOwner.getContext())
                    .load(item.getOwner().getAvatarUrl())
                    .into(((GithubMainVH)holder).imgOwner);
        } else {
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    public class GithubMainVH extends RecyclerView.ViewHolder {
        @BindView(R.id.txtItemName) TextView txtItemName;
        @BindView(R.id.txtItemDescription) TextView txtItemDescription;
        @BindView(R.id.txtForkCount) TextView txtForkCount;
        @BindView(R.id.txtStarCount) TextView txtStarCount;
        @BindView(R.id.imgOwner) ImageView imgOwner;
        @BindView(R.id.txtOwnerName) TextView txtOwnerName;

        public GithubMainVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBar) ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
