package com.example.jonathan.github.presenter;

import android.util.Log;

import com.example.jonathan.github.data.model.Item;
import com.example.jonathan.github.data.model.PullRequest;
import com.example.jonathan.github.data.network.api.GithubAPI;
import com.example.jonathan.github.view.PullRequestView;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jonathan on 24/11/16.
 */

public class PullRequestPresenter {
    private final String TAG = "PRPresenter";

    private PullRequestView mMainView;

    private ArrayList<PullRequest> mList;

    private Item mItem;

    public PullRequestPresenter(PullRequestView view, Item item) {
        mMainView = view;
        mItem = item;
        mList = new ArrayList();
    }

    public void load() {
        mMainView.onHideRetry();
        mMainView.onShowProgress();

        GithubAPI api = GithubAPI.getInstance();

        api.getPullRequestList(mItem.getOwner().getLogin(), mItem.getName())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<PullRequest>>() {
                    @Override
                    public void onCompleted() {
                        mMainView.onHideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        //
                        mMainView.onHideProgress();

                        mMainView.onShowRetry();

                        mMainView.onShowErrorMessage("");
                    }

                    @Override
                    public void onNext(List<PullRequest> pullRequests) {
                        setList(pullRequests);
                        Log.d(TAG, "onNext");
                    }
                });
    }

    public ArrayList<PullRequest> getList() {
        return mList;
    }

    public void setList(List<PullRequest> itemList) {
        if(itemList!= null && itemList.size() > 0) {
            mList.addAll(itemList);
            mMainView.onRenderData(mList);
        } else {
            mMainView.onRenderEmptyData();
        }
    }

    public void onItemClick(final int position) {
        mMainView.onItemSelected(mList.get(position));
    }
}
