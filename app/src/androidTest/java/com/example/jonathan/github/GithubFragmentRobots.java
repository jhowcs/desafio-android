package com.example.jonathan.github;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;

import com.example.jonathan.github.view.GithubActivity;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by jonathan on 5/27/17.
 */

public class GithubFragmentRobots {

    private final MockWebServer server;
    public final IntentsTestRule<GithubActivity> rule;

    public GithubFragmentRobots(MockWebServer server, IntentsTestRule<GithubActivity> rule) {
        this.server = server;
        this.rule = rule;
    }

    public GithubFragmentRobots withMockResponse(final int statusCode,
                                                 final String mockFile) throws Exception{
        server.enqueue(new MockResponse()
                .setResponseCode(statusCode)
                .setBody(TestHelper
                        .getStringFromFile(getInstrumentation().getContext(), mockFile)));

        return this;
    }

    public GithubFragmentRobots githubListRecyclerViewDisplayed() {
        onView(withId(R.id.rvGithubList)).check(matches(isDisplayed()));
        return this;
    }

    public GithubFragmentRobots fieldWithIdAndTextDisplayedInRecyclerView(final int id, final String text) {
        onView(allOf(withId(id), withText(text)))
                .check(matches(isDisplayed()));
        return this;
    }

    public GithubFragmentRobots fieldWithIdHasSiblingWithTextDisplayedInRecyclerView(final int id, final String text) {
        onView(allOf(withId(id),
                hasSibling(withText(text))))
                .check(matches(isDisplayed()));
        return this;
    }

    public GithubFragmentRobots githubRecyclerViewScrollToPosition(final int position) {
        onView(withId(R.id.rvGithubList))
                .perform(RecyclerViewActions.scrollToPosition(position));
        return this;
    }

    public GithubFragmentRobots clickOnViewWithText(final String text) {
        onView(withText(text)).perform(click());
        return this;
    }

    public GithubFragmentRobots serverWait() {
        TestHelper.serverWait();
        return this;
    }

    public GithubFragmentRobots startActivity() {
        rule.launchActivity(new Intent());
        return this;
    }

    public void checkNavigationToActivity(String activityName) {
        intended(hasComponent(activityName));
    }
}
