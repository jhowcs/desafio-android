package com.example.jonathan.github;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by jonathan on 4/1/17.
 */

public class TestHelper {

    private static final String TAG = "TestHelper";

    private static final long DEF_WAIT = 1500;

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(Context context, String filePath) throws Exception {
        final InputStream stream = context.getResources().getAssets().open(filePath);

        String ret = convertStreamToString(stream);
        //Make sure you close all streams.
        stream.close();
        return ret;
    }

    public static void serverWait() {
        serverWait(DEF_WAIT);
    }

    public static void serverWait(long timeToWait)  {
        try {
            Thread.sleep(timeToWait);
        } catch (InterruptedException e) {
            Log.e(TAG, "error on method serverwait " + e.getMessage());
        }
    }
}
