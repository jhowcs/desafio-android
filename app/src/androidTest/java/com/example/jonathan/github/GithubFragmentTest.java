package com.example.jonathan.github;

import android.content.Intent;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.jonathan.github.data.network.api.GithubAPI;
import com.example.jonathan.github.data.network.services.GithubService;
import com.example.jonathan.github.view.GithubActivity;
import com.example.jonathan.github.view.GithubDetailActivity;

import net.vidageek.mirror.dsl.Mirror;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class GithubFragmentTest {

    private static final String gitResponsePage1 = "git_response.json";
    private static final String gitResponsePage2 = "git_response_page2.json";
    private static final String pullRequestFile = "pull_request_response.json";

    private static final int HTTP_RESPONSE_OK = 200;
    private static final int HTTP_RESPONSE_404 = 404;

    @Rule
    public IntentsTestRule<GithubActivity> rule =
            new IntentsTestRule<>(GithubActivity.class, true, false);

    private MockWebServer server;

    private GithubFragmentRobots robots;

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        setupUrl();
        robots = new GithubFragmentRobots(server, rule);
    }

    @Test
    public void onAppStarts_ShouldShowRepositories() throws Exception {
        robots.withMockResponse(HTTP_RESPONSE_OK, gitResponsePage1)
                .startActivity()
                .serverWait()
                .githubListRecyclerViewDisplayed()
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtItemName, "RxJava")
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtItemDescription, "RxJava – Reactive Extensions for the JVM – a library for composin")
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtForkCount, "4046")
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtStarCount, "22911")
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtItemName, "RxJava")
                .fieldWithIdHasSiblingWithTextDisplayedInRecyclerView(R.id.imgOwner, "ReactiveX");
    }

    @Test
    public void onErrorLoadingData_ShouldDisplayARetryButton() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(HTTP_RESPONSE_404));

        rule.launchActivity(new Intent());

        onView(withId(R.id.imgRetry)).check(matches(isDisplayed()));
        onView(withText("Clique para tentar conectar novamente")).check(matches(isDisplayed()));
    }

    @Test
    public void onClickRepository_ShouldDisplayPullRequests() throws Exception {
        robots.withMockResponse(HTTP_RESPONSE_OK, gitResponsePage1)
                .withMockResponse(HTTP_RESPONSE_OK, pullRequestFile)
                .startActivity()
                .serverWait()
                .clickOnViewWithText("elasticsearch")
                .checkNavigationToActivity(GithubDetailActivity.class.getName());
    }

    @Test
    public void onScrollToTheEndOfList_ShouldFetchMoreData() throws Exception {
        final int lastPositionPage1 = 30;
        final int secondPositionPage2 = 32;

        robots.withMockResponse(HTTP_RESPONSE_OK, gitResponsePage1)
                .withMockResponse(HTTP_RESPONSE_OK, gitResponsePage2)
                .startActivity()
                .serverWait()
                .githubRecyclerViewScrollToPosition(lastPositionPage1)
                .serverWait()
                .githubRecyclerViewScrollToPosition(secondPositionPage2)
                .fieldWithIdAndTextDisplayedInRecyclerView(R.id.txtItemName, "androidannotations");
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    private void setupUrl() {
        String url = server.url("/").toString();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        final GithubService api = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build().create(GithubService.class);

        GithubAPI githubAPI = GithubAPI.getInstance();

        setField(githubAPI, "api", api);
    }

    private void setField(Object target, String fieldName, Object value) {
        new Mirror()
                .on(target)
                .set()
                .field(fieldName)
                .withValue(value);
    }
}